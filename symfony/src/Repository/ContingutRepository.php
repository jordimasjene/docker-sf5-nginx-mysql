<?php

namespace App\Repository;

use App\Entity\Contingut;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Contingut|null find($id, $lockMode = null, $lockVersion = null)
 * @method Contingut|null findOneBy(array $criteria, array $orderBy = null)
 * @method Contingut[]    findAll()
 * @method Contingut[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContingutRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contingut::class);
    }

    // /**
    //  * @return Contingut[] Returns an array of Contingut objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Contingut
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
