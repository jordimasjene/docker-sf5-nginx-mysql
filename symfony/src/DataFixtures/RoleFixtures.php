<?php

namespace App\DataFixtures;

use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class RoleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $count = 0;
        /** @var array $content */
        $content = [
            'ROLE_ADMIN',
            'ROLE_USER',
            'ROLE_ADMIN_GUEST',
        ];
        foreach ($content as $value) {
            $count++;
            $content = new Role();
            $content->setRoleName($value);
            $manager->persist($content);

            $this->addReference(Role::class.'_'.$count, $content);
        }

        $manager->flush();
    }

    /**
     * Set the reference entry identified by $name
     * and referenced to managed $object. If $name
     * already is set, it throws a
     * BadMethodCallException exception
     *
     * @see Doctrine\Common\DataFixtures\ReferenceRepository::addReference
     *
     * @param string $name
     * @param object $object - managed object
     *
     * @return void
     *
     * @throws BadMethodCallException - if repository already has a reference by $name.
     */
    public function addReference($name, $object)
    {
        $this->referenceRepository->addReference($name, $object);
    }
}
