<?php

namespace App\DataFixtures;

use DateTime;
use App\Entity\User;
use App\Entity\Role;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $role = $manager->getRepository(Role::class)->findOneBy(['roleName' => 'ADMIN']);

        $user = new User();
        $user->setUsername('admin');
        $user->setName('admin');
        $user->setSurname('Seidor');
        $user->setRoles($role);
        $user->setCreatedAt(new DateTime());
        $user->setUpdatedAt(new DateTime());

        $password = $this->encoder->encodePassword($user, '12345');
        $user->setPassword($password);

        $manager->persist($user);

        $manager->flush();
    }
}
