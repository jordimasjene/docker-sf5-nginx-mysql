<?php

namespace App\Service;

use Psr\Container\ContainerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;

class ServiceDataJson extends AbstractController
{
    const PATHDATA = '../data/';
    protected $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Undocumented function
     *
     * @param Request $request
     * @return array
     */
    public static function renderWithParams(Request $request, ContainerInterface $container)
    {
        $tema = new ServiceDataJson($container);
        return $tema->getTema($request->get('tema'));
    }


        /**
     * Undocumented function
     *
     * @param Request $request
     * @return void
     */
    public function renderService(Request $request)
    {
        $data = ServiceDataJson::renderWithParams($request, $this->container);

        return is_null($data) 
            ? $this->redirect($this->generateUrl('error'))
            : $this->render('formacio/tema/index.html.twig', [
            'controller_name' => 'FormacioController',
            'ContentData' =>  $data
        ]);
    }

    /**
     * Undocumented function
     *
     * @param string $tema
     * @return void
     */
    public function getTema(string $tema = null)
    {
        $filesystem = new Filesystem();

        try {
            if($filesystem->exists(self::PATHDATA.$tema.'.json')) {
                $package = new Package(new EmptyVersionStrategy());
                $dataJson = (is_null($data = file_get_contents($package->getUrl(self::PATHDATA.$tema.'.json'))))
                ? null
                : json_decode($data,1);
                
                return $dataJson;
            }
        } catch (IOExceptionInterface $exception) {
            return ['title' => 'El tema no existeix'];
        }
    }
}
