<?php

namespace App\Entity;

use App\Entity\Traits\HasTimestampsTrait;
use App\Repository\TemariRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TemariRepository::class)
 */
class Temari
{
    use HasTimestampsTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @ORM\OneToMany(targetEntity=Contingut::class, mappedBy="temari")
     */
    private $contents;

    public function __construct()
    {
        $this->contents = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Contingut[]
     */
    public function getContents(): Collection
    {
        return $this->contents;
    }

    public function addContents(Contingut $contents): self
    {
        if (!$this->contents->contains($contents)) {
            $this->contents[] = $contents;
            $contents->setTemari($this);
        }

        return $this;
    }

    public function removeContents(Contingut $contents): self
    {
        if ($this->contents->removeElement($contents)) {
            // set the owning side to null (unless already changed)
            if ($contents->getTemari() === $this) {
                $contents->setTemari(null);
            }
        }

        return $this;
    }
}
