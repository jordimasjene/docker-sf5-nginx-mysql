<?php

namespace App\Entity;

use App\Entity\Traits\HasTimestampsTrait;
use App\Repository\UserRepository;
use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Collection;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 */
class User implements UserInterface
{
    use HasTimestampsTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $username;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $password;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $surname;

        /**
     * Rol del usuario
     *
     * @var Role
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Role", inversedBy="regNumber")
     */
    private $role;

    /**
     * The salt to use for hashing.
     *
     * @var string
     */
    protected $salt;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     */
    protected $plainPassword;
    
    public function __construct()
    {
        $this->setCreatedAt(new \DateTime('now'));
        $this->setUpdatedAt(new \DateTime('now'));
    }
    
    /**
     * Retorna el nombre del role
     * @return string
     */
    public function __toString()
    {
        return (string)$this->role;
    }
    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function setId(int $Id): self
    {
        $this->Id = $Id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getUsername(): ?string
    {
        return $this->username;
    }

    public function setUsername(string $username): self
    {
        $this->username = $username;

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }
    
    /**
     * obtiene el rol del usuario
     * @return array
     */
    public function getRoles(): array
    {
        return [$this->getRoleName()];
    }
    
    /**
     * Get Role
     * @return string|null
     */
    public function getRole(): ?string
    {
        return $this->getRoleName();
    }
    
    public function getRoleName(): ?Role
    {
        return $this->role;
    }
    
    public function getRoleArray(): array
    {
        return $this->role;
    }
    
    /**
     * Set role
     * @param Role|array|null $role
     * @return $this
     */
    public function setRole(?array $role): self
    {
        
        $this->role = $role;

        return $this;
    }
    
    /**
     * Set role
     * @param Role|null $role
     * @return $this
     */
    public function setRoles(?Role $role): self
    {
        
        $this->role = $role;
        
        return $this;
    }
    
    
    /**
     * {@inheritdoc}
     */
    public function getSalt()
    {
        return $this->salt;
    }
    
    /**
     * {@inheritdoc}
     */
    public function setSalt($salt)
    {
        $this->salt = $salt;

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }
}
