<?php

namespace App\Entity;

use App\Entity\Traits\HasTimestampsTrait;
use App\Repository\ContingutRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ContingutRepository::class)
 */
class Contingut
{
    use HasTimestampsTrait;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Title;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Temari::class, inversedBy="no")
     */
    private $temari;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->Title;
    }

    public function setTitle(string $Title): self
    {
        $this->Title = $Title;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getTemari(): ?Temari
    {
        return $this->temari;
    }

    public function setTemari(?Temari $temari): self
    {
        $this->temari = $temari;

        return $this;
    }
}
