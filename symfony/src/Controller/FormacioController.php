<?php

namespace App\Controller;

use App\Service\ServiceDataJson;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class FormacioController extends AbstractController
{
    
    /**
     * @Route("/formacio", name="formacio")
     */
    public function index(): Response
    {
        return $this->render('formacio/index.html.twig', [
            'controller_name' => 'FormacioController',
        ]);
    }

    /**
     * @Route("/formacio/{tema}", name="formacio_tema")
     * 
     * Controlador amb parametres
     * 
     *
     * @param Request $request
     * @return void
     */
    public function withParams(Request $request)
    {
         return $this->render('formacio/tema/index.html.twig', [
            'controller_name' => 'FormacioController',
            'ContentData' => ServiceDataJson::renderWithParams($request, $this->container)
        ]);
    }

        /**
     * @Route("/formacio/tema/{tema}", name="formacio_service")
     * 
     * Controlador amb parametres
     * 
     * @param Request $request
     * @param ServiceDataJson $serviceDataJson
     * @return void
     */
    public function withParamsService(Request $request, ServiceDataJson $serviceDataJson)
    {
         return $serviceDataJson->renderService($request); 
    }
}
