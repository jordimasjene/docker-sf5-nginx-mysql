<?php

namespace App\Controller\Admin;

use App\Entity\Temari;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;

class TemariCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Temari::class;
    }

    /*
    public function configureFields(string $pageName): iterable
    {
        return [
            IdField::new('id'),
            TextField::new('title'),
            TextEditorField::new('description'),
        ];
    }
    */
}
