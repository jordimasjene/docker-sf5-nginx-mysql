<?php
    namespace App\EventSubscriber;
    
    use App\Entity\User;
    use App\Entity\Role;
    use Doctrine\ORM\EntityManagerInterface;
    use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeCrudActionEvent;
    use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityPersistedEvent;
    use EasyCorp\Bundle\EasyAdminBundle\Event\BeforeEntityUpdatedEvent;
    use Symfony\Component\EventDispatcher\EventSubscriberInterface;
    use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

    class EasyAdminSubscriber implements EventSubscriberInterface
    {
        private $em;
        private $passwordEncoder;
        
        public function __construct(EntityManagerInterface  $entityManager, UserPasswordEncoderInterface $passwordEncoder)
        {
            $this->em = $entityManager;
            $this->passwordEncoder = $passwordEncoder;
        }

        public static function getSubscribedEvents()
        {
            return [
                BeforeEntityPersistedEvent::class => ['setNewPostUser'],
                BeforeEntityUpdatedEvent::class   => ['setUpdatePostUser'],
            ];
        }
        
        public function setNewPostUser(BeforeEntityPersistedEvent $event)
        {
            $entity = $event->getEntityInstance();
            
            if (!($entity instanceof User)) {
                return;
            }
            $roleName = (is_array($entity->getRoleArray())) ? 'ROLE_USER' : $entity->getRoleArray()[0];
            $role = $this->em->getRepository(Role::class)->findOneBy(['roleName' => $roleName]);
            $entity->setRoles($role);
            $entity->setPassword($this->passwordEncoder->encodePassword($entity, $entity->getPassword()));
            if(is_null($entity->getCreatedAt())) {
                $entity->setCreatedAt(new \DateTime('now'));
            }
            if(is_null($entity->getUpdatedAt())) {
                $entity->setUpdatedAt(new \DateTime('now'));
            }
        }
    
        public function setUpdatePostUser(BeforeEntityUpdatedEvent $event)
        {
            $entity = $event->getEntityInstance();
        
            if (!($entity instanceof User)) {
                return;
            }
            $roleName = (is_array($entity->getRoleArray())) ? 'ROLE_USER' : $entity->getRoleArray()[0];
            $role = $this->em->getRepository(Role::class)->findOneBy(['roleName' => $roleName]);
            $entity->setRoles($role);
            $entity->setPassword($this->passwordEncoder->encodePassword($entity, $entity->getPassword()));
            if(is_null($entity->getUpdatedAt())) {
                $entity->setUpdatedAt(new \DateTime('now'));
            }
        }
    }